(ns ^:figwheel-always examples.basics.core
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [om-tick.bootstrap :refer [Date]]))

(enable-console-print!)

(println "Edits to this text should show up in your developer console.")

;; define your app data so that it doesn't get over-written on reload

(defonce app-state (atom {:text "Hello world!"
                          :date "1975-11-03"}))

(defn date-test [data owner]
  (reify om/IRender
    (render [_]
      (dom/div nil
             (dom/pre nil (pr-str (om/value data)))
             (om/build Date {:on-date-change #(om/update! data :date %)
                             :value           (:date data)})))))

(defn hello [data owner]
  (reify om/IRender
    (render [_]
      (dom/h1 nil (:text data)))))

(om/root
  date-test
  app-state
  {:target (. js/document (getElementById "app"))})


