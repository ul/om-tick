(ns om-tick.dev
    (:require
     [om-tick.core]
     [figwheel.client :as fw]
     om-tick.form-test
     om-tick.field-test
     om-tick.app-test
     [cemerick.cljs.test :refer [run-all-tests]]))

(fw/start {
  :websocket-url "ws://localhost:3450/figwheel-ws"
  :on-jsload (fn []
               ;; (stop-and-start-my app)
               (run-all-tests #"om-tick.*"))})

(run-all-tests #"om-tick.*")