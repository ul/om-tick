(ns om-tick.form
  (:require [om-tick.field :refer [field? reset-field initialise-field validate-field]]
            [om-tick.app :as app]))


(defn fmap [f m]
  (into (empty m) (for [[k v] m] [k (f v)])))


(defn form? [m]
  (contains? m :fields))


(defn reset-form [{:keys [fields] :as form}]
  (assoc form :fields (fmap reset-field fields)))


(defn load-data
  ([{:keys [data] :as form}]
    (load-data form data))
  ([form data]
   (reduce
     (fn [form-acc [k v]]
       (assoc-in form-acc [:fields k :value] v))
     form data)))


(defn extract-data
  "Extract current values from form as a map"
  [{:keys [fields]}]
  (into {} (->> fields (fmap :value))))


(defn initialise-form [form]
  {:pre [(form? form)]}
  (-> form
      (update :fields #(fmap initialise-field %))
      load-data))


(defn validate-fields
  [form]
  (update form :fields (partial fmap validate-field)))


(defn form-logic [form]
  (-> form validate-fields))


(defn form-app
  "
    The form app wrapps the basic form functionality up for reuse.

    * Only active if state map contains key k
    * Initialises form from payload
    * Apply an init-fn to payload if present
    * Apply standard form logic to derived state
    * Apply additional logic-fn if present
  "

  [k & {:keys [init-fn pre-init-fn init-logic? logic-fn form-logic?]
                     :or {init-logic? true
                          form-logic? true}}]
  (reify

    app/IAppActive
    (app-active? [_ state]
      (contains? state k))

    app/IInitState
    (init-state [_ payload]
      (cond-> payload
        (ifn? pre-init-fn) (update-in [k] pre-init-fn)
        init-logic? (update-in [k] initialise-form)
        (ifn? init-fn) (update-in [k] init-fn)))

    app/IDeriveState
    (derive-state [_ state]
      (cond-> state
        form-logic? (update-in [k] form-logic)
        (ifn? logic-fn) (update-in [k] logic-fn)))))