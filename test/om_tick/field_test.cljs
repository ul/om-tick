(ns om-tick.field-test
  (:require
    [cemerick.cljs.test :refer-macros [is deftest testing]]
    [om-tick.field :refer [mask-disabled-value]]))


(deftest test-mask-disabled-value
         (let [field1 {:value "asdf"}
               field2 {:value "asdf" :disabled true}]

           (testing "No affect when not disabled"
                    (is (= field1 (mask-disabled-value field1))))

           (testing "Value nil when disabled"
                    (is (= (mask-disabled-value field2)
                           (assoc field2 :value nil))))))

